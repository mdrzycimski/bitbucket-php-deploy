<?php 
/**
 * Deployment script to be run from bitbucket.  This script runs a shell 
 * script on the server to do deployment.    It should also run from github
 * with a change to the $_repositoryIp setting, and any other repository
 * that can call a URL on commit.
 * 
 * @author Iain Gray igray@itgassociates.com
 * 
 * Based on deployment script by Brandon Summers
 * http://brandonsummers.name/blog/2012/02/10/using-bitbucket-for-automated-deployments/
 * 
 *  
 * In addition to this script, you'll need a shell script to do your deployments, 
 * and to set up sudo to allow apache to run the script as your deploy user, as follows:
 * 
 * 1. Set up a new user & group 'deploy'
 * 2. Log in as root to your server, su to deploy and set up SSH deployment keys, then 
 *    add them to your bitbucket / github repository
 * 3. Git clone the repository using SSH to your application root
 * 4. Make deploy the owner of the repository, then set deploy's shell to nologin
 * 5. Edit the file git-deploy.sh, and replace [FOLDER]  with the location of your files
 * 6. Place git-deploy.sh into /usr/local/sbin.  Set owner to root, group to 
 *    deploy, and set permissions 750
 * 7. Make the changes to in sudo-change.txt to your /etc/sudoers file 
 *    (use visudo)  
 * 9. Place this script in a web directory outside of your repository folder
 *    (using a different virtual host is a good idea), and set up bitbucket/
 *    github to post to it every time there is a commit.
 * 
 *    
 *   
 * 
 *      
 * 
 * 
 * 
 */
date_default_timezone_set('Europe/London');

class Deploy {
    
    
  /**
  * A callback function to call after the deploy has finished.
  * 
  * @var callback
  */
  public $post_deploy;
  
  
  /**
   * The name of the deploy script to run
   * @var string
   */
  private $_deployScript;
  
  /**
   * The username to run the deployment under
   * @var string
   */
  private $_deployUser = 'deploy';
  
  /**
  * The name of the file that will be used for logging deployments. Set to 
  * FALSE to disable logging.  You need to create the deploy directory if it doesn't exist already.
  * 
  * @var string
  */
  private $_log = '/var/log/deploy/deployments.log';
  
  /**
   * Acceptable IPs for the remote respository - includes
   * Bitbucket IP, but you can also put other addresses in 
   * if you want to trigger the script from elsewhere.
   * 
   * @var array of IP addresses
   */
  private $_repositoryIp = array ( '63.246.22.222');

  /**
  * The timestamp format used for logging.
  * 
  * @link    http://www.php.net/manual/en/function.date.php
  * @var     string
  */
  private $_date_format = 'Y-m-d H:i:sP';
  
  /**
   * Address to email the results to
   * @var string
   */
  private $_email = 'user@example.com';
  
  
  /**
   * Whether or not to send an email with results
   * @var boolean
   */
  private $_sendEmail = true;

  /**
  * Sets up defaults.
  * 
  * @param  string  $directory  Directory where your website is located
  * @param  array   $data       Information about the deployment
  */
  public function __construct($directory, $options = array())
  {
      
      ini_set('display_errors', true);
      // Determine the directory path

      $available_options = array('log', 'date_format', 'email', 'repositoryIp');

      foreach ($options as $option => $value)
      {
          if (in_array($option, $available_options))
          {
              $this->{'_'.$option} = $value;
          }
      }  
      

      $this->log('Attempting deployment...');
  }

  /**
  * Writes a message to the log file.
  * 
  * @param  string  $message  The message to write
  * @param  string  $type     The type of log message (e.g. INFO, DEBUG, ERROR, etc.)
  */
  public function log($message, $type = 'INFO')
  {
      if ($this->_log)
      {
          // Set the name of the log file
          $filename = $this->_log;

          if ( ! file_exists($filename))
          {
              // Create the log file
              file_put_contents($filename, '');

              // Allow anyone to write to log files
              chmod($filename, 0666);
          }

          // Write the message into the log file
          // Format: time --- type: message
          file_put_contents($filename, date($this->_date_format).' --- '.$type.': '.$message.PHP_EOL, FILE_APPEND);
      }
  }

  /**
  * Executes the necessary commands to deploy the website.
  */
  public function execute()
  {
      try
      {
          if (!in_array($_SERVER['REMOTE_ADDR'], $this->_repositoryIp))
              throw new Exception($_SERVER['REMOTE_ADDR'].' is not an authorised Remote IP Address');	
              
          //run the deploy script
          exec("sudo -u {$this->_deployUser} {$this->_deployScript} 2>&1", $output, $return);
          
          if ($return!==0)
          {
              echo (implode(" ", $output));
              echo $return;
            throw new Exception("Error $return executing shell script");
          }
          else          
          {
            $this->log('Running deploy shell script... '.implode(' ', $output));	         
            unset ($output);
          }
          
          //log and email
          $this->log('Deployment successful.');
           if ($this->_sendEmail)
            mail($this->_email, 'Deployment script succeeded!', 'Check logs for details');          
      }
      catch (Exception $e)
      {
          //log and email
          $this->log($e, 'ERROR');  
          if ($this->_sendEmail)
            mail($this->_email, 'Deployment script failed', $e);          
      }
  }

}


// Usage
$deploy = new Deploy('/usr/local/sbin/git-deploy.sh');

//$deploy->post_deploy = function() use ($deploy) {
  // put any post deployment steps in here
//};

$deploy->execute();


?>
